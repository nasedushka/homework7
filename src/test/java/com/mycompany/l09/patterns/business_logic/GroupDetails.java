package com.mycompany.l09.patterns.business_logic;

import com.mycompany.l09.patterns.page_objects.GroupDetailsPage;

import static org.testng.Assert.assertEquals;

public class GroupDetails {

    private final GroupDetailsPage groupDetailsPage;

    GroupDetails(GroupDetailsPage groupDetailsPage) {
        this.groupDetailsPage = groupDetailsPage;
    }

    public GroupDetailsAssertion assertThat() {
        return new GroupDetailsAssertion();
    }

    public class GroupDetailsAssertion {

        public GroupDetailsAssertion groupNameIs(String expGroupName) {
            var actualGroupName = groupDetailsPage.getGroupName();
            assertEquals(actualGroupName, expGroupName,
                    "Invalid group name: " + actualGroupName);
            return this;
        }

    }
}
