package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class UserMainPage extends BasePage {

    @FindBy(xpath = "//a[@href='/groups']")
    private WebElement allGroupsLink;

    @FindBy(xpath = "//div[@title='Группы']")
    private WebElement groupsLink;

    @FindBy(xpath = "//div[@title='T-Store']")
    private WebElement tStoreIcon;

    @FindBy(xpath = "//a[@href='/shop']")
    private WebElement shopIcon;

    @FindBy(xpath = "//a[@href='/wikilist']")
    private WebElement companyNews;

    @FindBy(xpath = "//h3[text()='Внутренняя автоматизация: новости проекта']")
    private WebElement newsAboutInternalAutomation;

    @FindBy(xpath = "//div[@title='Приложения']")
    private WebElement appsIcon;

    @FindBy(xpath = "//a[@title='Объявления']")
    private WebElement noticeIcon;

   @FindBy(xpath = "//div[@title='Основное']")
    private WebElement mainMenu;

    @FindBy(xpath = "//a[@title='Коллеги']")
    private WebElement colleagues;

    public UserMainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public SearchGroupsPage clickGroups() {
        wait.until(elementToBeClickable(groupsLink)).click();
        wait.until(elementToBeClickable(allGroupsLink)).click();
        return new SearchGroupsPage(driver);
    }

    public TstorePage openStore() {
        wait.until(elementToBeClickable(tStoreIcon)).click();
        wait.until(elementToBeClickable(shopIcon)).click();
        sleep(20_000);
        return new TstorePage(driver);
    }

    public AnyNewsPage openNews() {
        wait.until(elementToBeClickable(companyNews)).click();
        wait.until(elementToBeClickable(newsAboutInternalAutomation)).click();
        return new AnyNewsPage(driver);
    }

    public NoticePage openNoticePage() {
        wait.until(elementToBeClickable(appsIcon)).click();
        wait.until(elementToBeClickable(noticeIcon)).click();
        return new NoticePage(driver);
    }

    public EmployeePage openEmployeePage() {
        wait.until(elementToBeClickable(mainMenu)).click();
        wait.until(elementToBeClickable(colleagues)).click();
        return new EmployeePage(driver);
    }

}
