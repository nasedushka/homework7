package com.mycompany.l09.patterns.page_objects;

import com.mycompany.l09.patterns.controls.Button;
import com.mycompany.l09.patterns.controls.Field;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddGroupPage extends BasePage {

    @FindBy(name = "name")
    private WebElement groupNameField;

    @FindBy(name = "description")
    private WebElement descriptionField;

    @FindBy(xpath = "//div[contains(text(),'Открытая')]//ancestor::label")
    private WebElement openedRadioButton;

    @FindBy(xpath = "//div[contains(text(),'Закрытая')]//ancestor::label")
    private WebElement closedRadioButton;

    @FindBy(xpath = "//div[@class='Dialog-Actions']//button[contains(text(),'Создать')]")
    private WebElement saveButton;

    AddGroupPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public AddGroupPage enterName(String name) {
        new Field(driver, groupNameField).enterText(name);
        return this;
    }

    public AddGroupPage enterDescription(String description) {
        new Field(driver, descriptionField).enterText(description);
        return this;
    }

    public AddGroupPage setOpen(boolean isOpen) {
        if (isOpen) {
            new Button(driver, openedRadioButton).click();
        } else {
            new Button(driver, closedRadioButton).click();
        }
        return this;
    }

    public GroupDetailsPage save() {
        new Button(driver, saveButton).click();
        sleep(1_000);
        return new GroupDetailsPage(driver);
    }
}
