package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class EmployeePage extends BasePage {

    @FindBy(css = ".SearchInput-Input")
    private WebElement searchField;

    @FindBy(css = ".List-Item-Avatar")
    private WebElement avatarIcon;

    @FindBy(css = ".List-Item-Avatar")
    private WebElement employeePic;

    public EmployeePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public EmployeePage searchColleague() {
        wait.until(elementToBeClickable(searchField)).click();
        searchField.sendKeys("Ишков");
        wait.until(elementToBeClickable(avatarIcon)).click();
        return new EmployeePage(driver);
    }

    public EmployeePage employeePageIsOpened() {
        wait.until(elementToBeClickable(employeePic));
        Assert.assertTrue(employeePic.isDisplayed());
        return new EmployeePage(driver);
    }

}
