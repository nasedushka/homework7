package com.mycompany.l09.patterns.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class NoticePage extends BasePage {

    public NoticePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".InfoBlock-Info")
    WebElement pageTitle;

    @FindBy(css = ".HeaderLayout_type_post")
    WebElement noticeTitle;

    @FindBy(css = ".ActionButton.NewsActions-Like .Icon.Icon_heart")
    WebElement likeIcon;

    @FindBy(css = ".ActionButton.NewsActions-Like .Icon.Icon_give")
    WebElement thankIcon;


    public NoticePage pageIsOpened() {
        wait.until(ExpectedConditions.visibilityOf(pageTitle));
        Assert.assertTrue(pageTitle.isDisplayed());
        Assert.assertTrue(pageTitle.getText().equalsIgnoreCase("Объявления"));
        return new NoticePage(driver);
    }

    public NoticePage isNoticeDisplayed() {
        Assert.assertTrue(noticeTitle.isDisplayed());
        Assert.assertTrue(likeIcon.isDisplayed());
        Assert.assertTrue(thankIcon.isDisplayed());
        return new NoticePage(driver);
    }

}
