package com.mycompany.l09.patterns.app;

import com.mycompany.l09.patterns.business_logic.Intra;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Application {

    private WebDriver webDriver;
    private String browser;

    public Application(String browser) {
        this.browser = browser;
    }

    public WebDriver getWebDriver() {
        if (webDriver == null) {
            webDriver = WebDriverFactory.getInstance(browser);
            webDriver.manage().window().maximize();
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            webDriver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        }
        return webDriver;
    }

    public Intra getIntra() {
        return new Intra(this);
    }

    public void stop() {
        if (webDriver != null) {
            webDriver.quit();
        }
    }
}
