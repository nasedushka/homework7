package com.mycompany.l09.patterns.domain;

import java.util.Objects;

public class Group {

    private String name;

    private String description;

    private boolean isOpen;

    public String getName() {
        return name;
    }

    public Group withName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Group withDescription(String description) {

        this.description = description;
        return this;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public Group open(boolean isOpen) {
        this.isOpen = isOpen;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return isOpen == group.isOpen &&
                Objects.equals(name, group.name) &&
                Objects.equals(description, group.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, isOpen);
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", isOpen=" + isOpen +
                '}';
    }
}
